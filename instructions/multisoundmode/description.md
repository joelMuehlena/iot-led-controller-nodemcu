# MultiSoundLedMode

This LEDMode can be used to sync a sound reactive state between multiple controllers. One controller with a microphone needs to act as a server. Other controllers which act as a client can discover the server and connect to it via WebSocket. They then will receive the state of the LEDs of the server and look all the same.
Can be used to sync multiple controllers maybe in different rooms to a controller next for example a sound source. The name of this mode is **MULTISOUNDLEDMODE**.

## Configure

## How it works
