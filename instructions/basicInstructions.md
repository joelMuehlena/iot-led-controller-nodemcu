# Basic instructions and controlling of the controller

JSON Schemes used for communication and other Protocols used and defined are explained here.

This LEDController is using WebSocket connections as its main communication channel. It also supports REST Endpoints for some not expensive operations which don't need the presence of this type of bidirectional communication.

The WebSocket connection is only available if the controller is already configured into a Wifi network and has connected itself successfully to it.

The section [Configure](#Configure-the-Controller) holds the description of all Commands and possibilities to configure the LEDController via its WebSocket connection.

The section [Change LED Mode](#Change-the-LEDMode) explains how to change the LEDMode of the Controller. How to configure a specific LEDMode is defined in the instruction file matching the LEDMode name.

## WebSocket interface of the LEDController

The LEDController exposes a WebSocket interface normally on port 81. It uses channels for communication.
If you want to know more about how we use channels look at the section [Channel communication](#Channel-communication-via-the-WebSocket-protocol). The WebSocket interface allows clients to connect and send commands for configuration and LEDModes in a more simple and intuitive way then just over HTTP. It also allows a higher throughput of data and a bidirectional communication. Web-interfaces and apps for the controller should use its WebSocket interface. Excluded from this are some simple REST requests like /healthz or /name.

### Channel communication via the WebSocket protocol

I wrote a wrapper for the WebSocket library to easier communicate via WebSockets protocol and only have one main instance created at the startup of the controller. Any part of the controller can subscribe to channels and register a callback function to be called after receiving on this channel. To structure the WebSocket payload easy and to determine the channel and data sections I decided to use JSON. A basic message for the communication with the LEDController via its WebSocket interface look like the following:

```json
{
  "channel": "channelName", // The name of the channel you want the message submit to
  "isBroadcast": false, // If the message is meant for all channels
  "data": { // Holds the data of the payload you define what to send here but it should always be an object
    "theData": "you want to send",
    "moreData": 23
  }
}
```

## Configure the Controller

The controller has a channel called config. It should receive all LEDController specific and generic config commands and operations.

Available commands and operations:

- [key: controllerState, value: [on | off]: string] => Sets the controller state to on or off (turns the light off or back on) **After this command no other command will be handled the function returns**
- [key: brightness, value: [<0 - 100>]: int] => Sets the brightness of the led controller (just works for modes which don't configure their own brightness level)
- [key: controllerName, value: [any]: string] => Sets the name of the controller to the given string

```json
// Example config data 

// Turn brightness to 23% and set the controller name to esp-led-controller
{
  "channel": "config",
  "isBroadcast": false,
  "data": {
    "brightness": 23,
    "controllerName": "esp-led-controller"
  }
}

// Turn controller off
{
  "channel": "config",
  "isBroadcast": false,
  "data": {
    "controllerState": "off"
  }
}
```

If no generic config is found the config data object will be redirected to the current LEDMode which maybe can handle the send data.

## Change the LEDMode

There are two ways to change the LEDControllers LEDMode.

1. First is a simple REST Api request to the ip of the controller. The endpoint is: /mode and the request method is POST. The request requires a body with the key modeName and a value with the name of the LEDMode. The body should be application/x-www-form-urlencoded.
2. Second is via the WebSocket connection. If not already connected to the WebSocket server of the LEDController connect to it now. After connection you have to send a msg to the channel if you are not sure how to send to a channel look at the section [Channels via WebSockets](#Channel-communication-via-the-WebSocket-protocol) **modeChange** with the following data:

```json
// Just the data Section
{
    "newModeName": "<modeName>"
}

// Full example
{
    "channel": "channelName",
    "isBroadcast": false,
    "data": {
        "newModeName": "<modeName>"
    }  
}
```
