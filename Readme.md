# ESP8266 LED-Controller

This project is a led controller for esp8266 (esp32 support will follow some day). It works with analog led stripes. It supports some LEDModes out of the box. E.g. Static Color, Faded Colors or Sound Reactive Colors. The project aims to support simple addition of own led modes to do so check [Add own LEDMode](#Add-own-LEDMode).

## Installation & Usage

If you want to use this project you just need to clone it and edit the `defines.h` file to match your GPIO pins. If you don't have a microphone just remove the line. After adjusting the defines you can flash the software to the microcontroller. After flashing you need to upload the files inside of the *data* directory to the controller. The controller uses LitlleFS. If you skip this step you wont be able to connect the controller to a wifi network.

## Add own LEDMode

To add a new LEDMode you need to create a new class which inherits the LEDMode base class. You need to overwrite `void handleMode()` and `void configMode(DynamicJsonDocument &configData)`. The first one is called every loop to handle e.g. changing colors to an event (see `FadeMode.h/cpp`). The second one is called if a WebSocket event on the config channel is received and the configuration is not a generic one for the controller. See *instructions/basicInstructions.md* Configure the Controller for more info.

The following example creates an led mode with the name MYLEDMODE. Every loop it will check for the `myOwnModeVar`. If it is true it will send a message to the LEDController to set the color to `myColor`. Otherwise it will set the color to off.

```cpp

#include "RGBColor.h"
#include "LEDMode.h"

class MyLedMode : public LEDMode
{
private:
    bool myOwnModeVar;
    RGBColor myColor;
public:
    
    MyLedMode() : LEDMode("MYLEDMODE"), myOwnModeVar(true), myColor(RGBColor(255, 186, 3)) {}

    void handleMode() override 
    {
        if(this->myOwnModeVar) {
            LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, this->myColor);
            LEDMode::notify(msg);
        }else {
            LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, LEDController::offColor);
            LEDMode::notify(msg);
        }
    }
    
    void configMode(DynamicJsonDocument &configData) override {}

    void myOwnMethod() 
    {
        this->myOwnModeVar = !this->myOwnModeVar;
    }
}

```
