#include "util.h"

namespace util
{
  RGBColor hexToRgb(int hexValue)
  {
    RGBColor color;

    color.r = ((hexValue >> 16) & 0xFF);
    color.g = ((hexValue >> 8) & 0xFF);
    color.b = ((hexValue)&0xFF);

    return color;
  }

  int max3(int x0, int x1, int x2)
  {
    int max = x2;

    if (x0 > x1)
    {
      if (x0 > x2)
        max = x0;
    }
    else
    {
      if (x1 > x2)
        max = x1;
    }

    return max;
  }

  unsigned int abs(int a, int b)
  {
    return ((a > b) ? (a - b) : (b - a));
  }

  RGBColor hexToRgb(String s)
  {
    s.replace("#", "");
    s.trim();
    int i = (int)strtol(s.c_str(), nullptr, 16);
    return hexToRgb(i);
  }
}
