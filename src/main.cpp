#include <WiFi.h>
#include <WebSocketsServer.h>
#include <Esp.h>
#include <ArduinoOTA.h>

#include <FS.h>
#include <LittleFS.h>
#include <ArduinoJson.h>

#include "defines.h"

#include "LittleFSWrapper.h"

#include "LEDController.h"
#include "Ardyt.h"
#include "configModeRestMethods.h"
#include "methods.h"
#include "fadeModeRESTMethods.h"
#include "singleSoundModeRESTMethods.h"
#include "LedModeHelper.h"

#define clg(x) Serial.println(x);
#define timeoutDelay 2

#define FORMAT_LITTLEFS_IF_FAILED true

bool CONFIGMODE = false;

void mqttCallback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Received message [");
  Serial.print(topic);
  Serial.print("] ");
  char msg[length + 1];
  for (int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
    msg[i] = (char)payload[i];
  }
  Serial.println();
}

void wsModeChanger(DynamicJsonDocument data, WebSocket::ReturnChannelCb sendBack)
{

  WebSocket::unused_uint8_t unused = 0;

  const char *modeName = data["modeName"];
  if (strcmp(modeName, "null") == 0)
  {
    sendBack(unused, "error changing mode");
    return;
  }

  if (LEDModeHelper::changeLEDMode(modeName))
  {

    sendBack(unused, "{\"channel\":\"modeChange\",\"isBroadcast\":false,\"data\":{\"newModeName\":\"" + String(modeName) + "\"}}");
    sendBack(unused, LEDController::getInstance()->createConnectConfig());
  }
  else
  {
    sendBack(unused, "Failed to change to: " + String(modeName));
  }
};

void wsConfig(DynamicJsonDocument data, WebSocket::ReturnChannelCb sendBack)
{

  WebSocket::unused_uint8_t unused = 0;

  String cfgData = data["controllerState"];

  if (cfgData != "null")
  {
    if (cfgData != "off" && cfgData != "on")
    {
      sendBack(unused, "{\"channel\":\"config\",\"isBroadcast\":false,\"data\":{\"error\":\"Please provide a valid power state\"}}");
      return;
    }

    LEDController::getInstance()->setState(cfgData == "on" ? LEDController::State::ON : LEDController::State::OFF);
    return;
  }

  cfgData = data["brightness"].as<String>();

  if (cfgData != "null")
  {
    int levelInt = cfgData.toInt();
    LEDController::getInstance()->setBrightness(levelInt);
  }

  cfgData = data["controllerName"].as<String>();

  if (cfgData != "null")
  {
    LEDController::getInstance()->setName(cfgData);
  }

  LEDController::getInstance()->getLEDMode()->configMode(data);
};

void onWsConnect(WebSocket::ReturnChannelCb sendBack)
{
  WebSocket::unused_uint8_t unused = 0;
  sendBack(unused, "Connected");
  sendBack(unused, LEDController::getInstance()->createConnectConfig());
}

void setup()
{
  Serial.begin(9600);
  Serial.println();

  WiFi.persistent(false);

  if (!LittleFS.begin())
  {
    Serial.println("LITTLEFS Mount fehlgeschlagen");
  }
  else
  {
    Serial.println("LITTLEFS Mount erfolgt");
    LittleFSWrapper::listDir(LittleFS, "/", 3);
    Serial.println();
  }

  DynamicJsonDocument configDoc(2048);
  bool configFile = LittleFSWrapper::readJSONFile(configDoc, LittleFS, "/config.json");
  serializeJsonPretty(configDoc, Serial);

  LEDController::getInstance()->setAllPins(REDLED, BLUELED, GREENLED);
  LEDController::getInstance()->Init();

  if (!configFile)
  {
    CONFIGMODE = true;
    // Start in config mode
    Serial.println("Started in config mode");

    // Start AP
    IPAddress local_IP(192, 168, 4, 22);
    IPAddress gateway(192, 168, 4, 9);
    IPAddress subnet(255, 255, 255, 0);

    WiFi.mode(WIFI_AP_STA);
    WiFi.disconnect();
    delay(40);

    WiFi.softAPConfig(local_IP, gateway, subnet);
    if (!WiFi.softAP("ESP_LED_CONTROLLER", "password", 1, false, 2))
    {
      Serial.println("FAILED TO START AP");
      ESP.restart();
      return;
    }

    Serial.print("AP IP: ");
    Serial.println(WiFi.softAPIP());

    Ardyt *server = Ardyt::getInstance().get();
    server->setPort(80);
    server->Run(true);
    server->Get("/", [](const HTTPRequest &request, HTTPResponse &response)
                {
                        char *buffer = LittleFSWrapper::readFile(LittleFS, "/index.html");
                        response.setStatus(200)->addHeader("Content-Type", "text/html")->send(buffer);
                        delete[] buffer; });

    server->Get("/style.css", [](const HTTPRequest &request, HTTPResponse &response)
                {
                        char *buffer = LittleFSWrapper::readFile(LittleFS, "/style.css");
                        response.setStatus(200)->addHeader("Content-Type", "text/css")->send(buffer);
                        delete[] buffer; });

    server->Get("/script.js", [](const HTTPRequest &request, HTTPResponse &response)
                {
                        char *buffer = LittleFSWrapper::readFile(LittleFS, "/script.js");
                        response.setStatus(200)->addHeader("Content-Type", "application/javascript")->send(buffer);
                        delete[] buffer; });

    server->Get("/ssids", getSSIDS);
    server->Post("/credentials", postCredentials);
    server->Get("/wifiState", getWiFiStateAndSaveConfig);
  }
  else
  {
    CONFIGMODE = false;
    String ssid = configDoc["wifiSSID"];
    String pass = configDoc["wifiPassword"];

    if (ssid == NULL || pass == NULL || ssid.length() == 0 || pass.length() == 0 || pass == "null" || ssid == "null")
    {
      Serial.println("WIFI configuration is invalid resetting to config mode");
      LittleFSWrapper::deleteFile(LittleFS, "/config.json");
      delay(200);
      ESP.restart();
    }

    Serial.println("Started in normal mode");

    Ardyt *server = Ardyt::getInstance().get();
    server->setWiFiCredentials(ssid, pass);
    server->setPort(80);
    server->Run();

    // Configure Websocket
    server->setWSPort(81);
    server->getWS()->onConnect(&onWsConnect);
    server->getWS()->subscribe("modeChange", &wsModeChanger);
    server->getWS()->subscribe("config", &wsConfig);

    // Configure MQTT
    server->addMQTTCallback(mqttCallback);

    // Setup HTTP Routes
    server->Get("/healthz", [](const HTTPRequest &request, HTTPResponse &response)
                { response.setStatus(200)->send("Controller HTTP is healthy. Everything should be ok"); });
    server->Post("/level", postLevel);
    server->Post("/color", postColor);
    server->Get("/mode", getLEDMode);
    server->Post("/mode", setLEDMode);
    server->Get("/name", [](const HTTPRequest &request, HTTPResponse &response)
                { response.setStatus(200)->send(LEDController::getInstance()->getName()); });

    server->Post("/fade", fadeModeOptions);

    server->Post("/singleSound", singleSoundOptions);
    server->Post("/multiSound", multiSoundOptions);

    // Reapply led config
    JsonVariant controllerConfig = configDoc["controllerConfig"];
    if (!controllerConfig.isNull() && controllerConfig.is<JsonObject>())
    {
      String brightness = controllerConfig["brightness"];
      const char *currentMode = controllerConfig["currentMode"];
      String currentColor = controllerConfig["currentColor"];
      String controllerName = controllerConfig["controllerName"];

      if (controllerName != "null")
      {
        LEDController::getInstance()->setName(controllerName);
      }

      if (currentMode != "null")
      {
        LEDModeHelper::changeLEDMode(currentMode);
      }

      if (currentColor != "null")
      {
        LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, util::hexToRgb(currentColor.toInt()));
        LEDController::getInstance()->getLEDMode()->notify(msg);
      }

      if (brightness != "null")
      {

        LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::BRIGHTNESS, brightness.toInt());
        LEDController::getInstance()->getLEDMode()->notify(msg);
      }
    }
  }
}

long prevHeapMillis = 0;
long interval = 30 * 1000;

void loop()
{

  long currentMillis = millis();

  if (currentMillis - prevHeapMillis > interval)
  {
    Serial.print("[ESP Core] HEAP MEM loop: ");
    Serial.println(ESP.getFreeHeap(), DEC);
    prevHeapMillis = currentMillis;
  }

  Ardyt *server = Ardyt::getInstance().get();

  server->loop(!CONFIGMODE, !CONFIGMODE, true);

  // Take some time to apply changes
  delayMicroseconds(50);

  LEDController::getInstance()->trigger();
}
