#include "Ardyt.h"
#include "LEDController.h"

std::shared_ptr<Ardyt> Ardyt::instance = nullptr;

Ardyt::Ardyt() : server(WiFiServer(80)), webSocket(WebSocket(0)), discoveryService(DiscoveryService(6323)), wifiClient(WiFiClient()), mqttClient(wifiClient), specificChannelName("/iot/led/" + LEDController::UUIDESPCONTROLLER), ssid(""), password("")
{
  server.close();
}

std::shared_ptr<Ardyt> Ardyt::getInstance()
{
  if (Ardyt::instance.get() == nullptr)
  {
    Ardyt::instance = std::shared_ptr<Ardyt>(new Ardyt());
  }

  return Ardyt::instance;
}

void Ardyt::Run(bool configMode)
{

  this->isConfigMode = configMode;

  if (!configMode)
    connectToWifi();
  this->startServer();
}

void Ardyt::connectToWifi()
{
  if (this->ssid.length() == 0 || this->password.length() == 0)
  {
    Serial.println("No WiFi credentials set");
    return;
  }
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.setSleep(false);
  WiFi.setAutoReconnect(false);
  WiFi.begin(this->ssid.c_str(), this->password.c_str());

  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());
}

void Ardyt::startServer()
{
  if (!this->isServerrunning && this->port > 0)
  {
    this->isServerrunning = true;
    this->server = WiFiServer(this->port);
    server.begin();

    Serial.println("Server started");
    Serial.print("Use this URL to connect: ");
    Serial.print("http://");
    Serial.print(WiFi.localIP());
    Serial.println("/");

    if (!CONFIGMODE)
    {
      this->mqttClient.setServer("test.mosquitto.org", 1883);
      reconnectMQTT();
    }
  }
}

bool Ardyt::reconnectMQTT()
{
  int count = 0;
  while (!mqttClient.connected())
  {

    if (++count >= 4)
    {
      Serial.println("MQTT Connection Failed");
      return false;
    }

    Serial.print("Reconnecting MQTT...");
    if (!mqttClient.connect(LEDController::UUIDESPCONTROLLER.c_str()))
    {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" retrying in 2.5 seconds");
      delay(2500);
    }
  }

  // Sub to general channel
  String tmp = this->specificChannelName;
  mqttClient.subscribe(tmp.c_str());

  // Sub to static channel
  tmp += "/staticMode";
  mqttClient.subscribe(tmp.c_str());

  // Sub to fade channel
  tmp = this->specificChannelName;
  tmp += "/fadeMode";
  mqttClient.subscribe(tmp.c_str());

  // Sub to multiSoundchannel
  tmp = this->specificChannelName;
  tmp += "/multiSound";
  mqttClient.subscribe(tmp.c_str());

#ifdef HAS_MIC
  // Sub to sound channel
  tmp = this->specificChannelName;
  tmp += "/singleSound";
  mqttClient.subscribe(tmp.c_str());
#endif

  Serial.println("Reconnected MQTT");
  return true;
}

void Ardyt::setWSPort(uint16_t port)
{
  this->wsPort = port;
  auto socket = WebSocket(port);
  this->webSocket = std::move(socket);
  // this->webSocket.setPort(port);
}

uint16_t Ardyt::getWSPort()
{
  return this->wsPort;
}

WebSocket *Ardyt::getWS()
{
  return &(this->webSocket);
}

void Ardyt::loop()
{
  this->loop(true, true, true);
}

#ifndef timeoutDelay
#define timeoutDelay 2
#endif

void Ardyt::loop(bool loopWs, bool loopMQTT, bool handleHttp)
{

  this->discoveryService.loop();

  if (!this->isConfigMode)
  {
    unsigned long currentMillis = millis();
    if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >= interval))
    {
      Serial.print("[ESP Core] HEAP MEM disconnected wifi: ");
      Serial.println(ESP.getFreeHeap(), DEC);
      Serial.println("Reconnecting to WiFi...");
      WiFi.disconnect();
      this->connectToWifi();
    }
  }

  if (handleHttp)
  {
    WiFiClient client = this->server.available();
    delayMicroseconds(10);

    if (client)
    {

      long timeoutMs = 0;
      while (timeoutMs < timeoutDelay * 1000 && !client.available())
      {
        delay(1);
        ++timeoutMs;
      }

      if (timeoutMs < timeoutDelay * 1000)
      {
        Serial.println("[LOG] New REST Request");
        HTTPRequest req = HTTPRequest(&client);
        this->handleRequest(req);
      }
      else
      {
        Serial.print("[LOG] ");
        Serial.print(timeoutDelay * 1000);
        Serial.println(" Seconds HTTP client Timeout");
      }
    }
  }

  if (loopWs)
  {
    this->webSocket.loop();
  }

  if (loopMQTT && false)
  {
    if (!mqttClient.connected())
    {
      loopMQTT = this->reconnectMQTT();
    }

    if (loopMQTT)
    {
      mqttClient.loop();
    }
  }
}

const String &Ardyt::getMQTTSpecificName() const
{
  return this->specificChannelName;
}

PubSubClient &Ardyt::getMQTTClient()
{
  return this->mqttClient;
}

void Ardyt::addMQTTCallback(void (*callback)(char *topic, byte *payload, unsigned int length))
{
  this->mqttClient.setCallback(*callback);
}

void Ardyt::setPort(int port)
{
  this->port = port;
}

void Ardyt::setWiFiCredentials(String ssid, String password)
{
  this->ssid = ssid;
  this->password = password;
}

String Ardyt::getWiFiSSID() const
{
  return this->ssid;
}

String Ardyt::getWiFiPass() const
{
  return this->password;
}

WiFiClient Ardyt::available()
{
  return this->server.available();
}

void Ardyt::Post(String path, HttpHandlerFunc method)
{
  auto r = std::make_shared<Route>(path, HTTPMethod::POST, method);
  routes.push_back(r);
}

void Ardyt::Get(String path, HttpHandlerFunc method)
{
  auto r = std::make_shared<Route>(path, HTTPMethod::GET, method);
  routes.push_back(r);
}

void Ardyt::handleRequest(const HTTPRequest &request)
{

  logRequest(request);

  HTTPResponse response = HTTPResponse(request.getClientPtr());

  if (this->isCorsEnabled)
  {
    response.addHeader("Connection", "keep-alive");
    response.addHeader("Access-Control-Allow-Origin", "*");
    response.addHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    response.addHeader("Access-Control-Allow-Headers", "Content-Type");
  }

  for (const auto &routeSP : this->routes)
  {

    Route *route = routeSP.get();

    if (route->getMethod() == request.getMethod() && route->getPath() == request.getRequestPath())
    {
      route->runHandler(request, response);
      return;
    }

    if (route->getPath() == request.getRequestPath() && request.getMethod() == HTTPMethod::OPTIONS && this->isCorsEnabled)
    {
      response.setStatus(204);
      response.addHeader("Connection", "keep-alive");
      response.addHeader("Access-Control-Allow-Origin", "*");
      response.addHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
      response.addHeader("Access-Control-Allow-Headers", "Content-Type");
      response.addHeader("Access-Control-Max-Age", "86400");
      response.send("");
      return;
    }
  }

  response.setStatus(404)->send("404 - Not Found");
}

void Ardyt::logRequest(const HTTPRequest &request)
{
  Serial.println(request.getRequestPath());
  Serial.println(HTTPRequest::getStringFromMethod(request.getMethod()));
  for (const auto &c : request.getBody())
  {
    Serial.print(c.first);
    Serial.print(" , ");
    Serial.println(c.second);
  }
}
