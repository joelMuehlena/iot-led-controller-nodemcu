#include "HTTPResponse.h"

HTTPResponse::HTTPResponse(WiFiClient *const client) : client(client), headers(std::map<String, String>()) {}

void HTTPResponse::finishRequest()
{
  this->isFinished = true;
  this->areHeadersWritten = true;
  this->client->flush();
}

HTTPResponse *const HTTPResponse::setStatus(int code)
{
  if (!this->isFinished)
  {
    String responseBegin = "HTTP/1.1 ";
    responseBegin += code;
    responseBegin += " ";
    responseBegin += HTTPResponse::getTextFromHTTPCode(code);
    client->println(responseBegin);
  }

  return this;
}

void HTTPResponse::send(String text)
{

  if (!this->isFinished)
  {
    if (!text.isEmpty() && this->headers.find("Content-Type") == this->headers.end())
    {
      addHeader("Content-Type", "text/html");
    }

    writeHeaders();

    if (!text.isEmpty())
    {
      this->client->println(text);
    }

    finishRequest();
  }
}

void HTTPResponse::send(const char *text)
{

  if (!this->isFinished)
  {
    if (text != nullptr && this->headers.find("Content-Type") == this->headers.end())
    {
      addHeader("Content-Type", "text/html");
    }

    writeHeaders();

    if (text != nullptr)
    {
      this->client->println(text);
    }

    finishRequest();
  }
}

boolean HTTPResponse::writeHeaders()
{
  if (!this->areHeadersWritten)
  {
    for (const auto &h : this->headers)
    {
      String headerLine = h.first;
      headerLine += ": ";
      headerLine += h.second;
      this->client->println(headerLine);
    }
    this->client->println("");
    this->client->println("");
    this->client->println("");
    this->areHeadersWritten = true;
    return true;
  }
  return false;
}

HTTPResponse *const HTTPResponse::addHeader(String key, String value)
{
  this->headers.insert(std::make_pair(key, value));
  return this;
}

String HTTPResponse::getTextFromHTTPCode(const int code)
{
  switch (code)
  {
  case 200:
    return "OK";
  case 204:
    return "No Content";
  case 400:
    return "Bad Request";
  case 404:
    return "Not Found";
  default:
    return "ERROR";
  }
}

void HTTPResponse::json(DynamicJsonDocument &json)
{
  if (!this->isFinished)
  {
    addHeader("Content-Type", "application/json");
    writeHeaders();
    String retSer = "";
    serializeJson(json, retSer);
    this->client->println(retSer);

    finishRequest();
  }
}
