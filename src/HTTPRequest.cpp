#include "HTTPRequest.h"

HTTPRequest::HTTPRequest(WiFiClient *client) : client(client), headers(std::map<String, String>()), body(std::map<String, String>())
{
  parseRequest();
  parseHeaders();
  parseBody();
}

void HTTPRequest::parseBody()
{
  auto lengthHeader = headers.find("Content-Length");
  auto contentTypeHeader = headers.find("Content-Type");

  if (lengthHeader != headers.end() && contentTypeHeader != headers.end())
  {

    if (!(contentTypeHeader->second.indexOf("multipart/form-data") != -1 || contentTypeHeader->second.indexOf("application/x-www-form-urlencoded") != -1))
    {
      Serial.println("Content Type not supported");
      return;
    }

    unsigned int contentLegnth = lengthHeader->second.toInt() + 1;

    if (contentLegnth - 1 < 1)
    {
      return;
    }

    char buffer[contentLegnth];

    client->readBytes(buffer, contentLegnth);

    this->contentString = "";
    for (int i = 0; i < contentLegnth; ++i)
    {
      this->contentString += buffer[i];
    }
    this->contentString.trim();

    String contentStringCpy = this->contentString;
    if (contentTypeHeader->second.indexOf("application/x-www-form-urlencoded") != -1)
    {
      while (true)
      {
        String key, value;

        if (contentStringCpy.indexOf("=") == -1)
        {
          break;
        }

        key = contentStringCpy.substring(0, contentStringCpy.indexOf("="));
        value = contentStringCpy.substring(contentStringCpy.indexOf("=") + 1, contentStringCpy.indexOf("&") == -1 ? contentStringCpy.length() : contentStringCpy.indexOf("&"));

        key.trim();
        value.trim();

        urldecode(key);
        urldecode(value);

        this->body.insert(std::make_pair(key, value));

        if (contentStringCpy.indexOf("&") == -1)
        {
          break;
        }
        contentStringCpy = contentStringCpy.substring(contentStringCpy.indexOf("&") + 1);
      }
    }

    if (contentTypeHeader->second.indexOf("multipart/form-data") != -1)
    {
    }
  }
}

void HTTPRequest::urldecode(String &string)
{
  string.replace("+", " ");
  string.replace("%20", " ");
  string.replace("%21", "!");
  string.replace("%22", "\"");
  string.replace("%23", "#");
  string.replace("%24", "$");
  string.replace("%25", "%");
  string.replace("%26", "&");
  string.replace("%27", "'");
  string.replace("%28", "(");
  string.replace("%29", ")");
  string.replace("%2A", "*");
  string.replace("%2B", "+");
  string.replace("%2C", ",");
  string.replace("%2D", "-");
  string.replace("%2E", ".");
  string.replace("%2F", "/");
  string.replace("%3A", ":");
  string.replace("%3B", ";");
  string.replace("%3C", "<");
  string.replace("%3D", "=");
  string.replace("%3E", ">");
  string.replace("%3F", "?");
  string.replace("%40", "@");
  string.replace("%5B", "[");
  string.replace("%5C", "\\");
  string.replace("%5D", "]");
  string.replace("%7B", "{");
  string.replace("%7C", "|");
  string.replace("%7D", "}");
}

void HTTPRequest::parseRequest()
{
  String request = client->readStringUntil('\r');
  request.trim();
  client->flush();

  int cnt = 0;
  while (true)
  {
    int index = request.indexOf(" ");

    String s = request.substring(0, index);
    s.trim();

    request = request.substring(index + 1);
    request.trim();

    if (cnt == 0)
    {
      this->method = HTTPRequest::getMethodFromString(s);
      if (this->method == HTTPMethod::METHOD_NOT_ALLOWED)
      {
        this->hasError = true;
        break;
      }
    }
    else if (cnt == 1)
    {
      this->requestPath = s;
    }
    else
    {
      this->httpVersion = s;
    }

    if (index == -1)
    {
      break;
    }

    ++cnt;
  }
}

void HTTPRequest::parseHeaders()
{
  while (true)
  {
    String line = client->readStringUntil('\r');
    line.trim();
    if (line.isEmpty())
    {
      client->flush();
      return;
    }

    String headerName = line.substring(0, line.indexOf(":"));
    headerName.trim();
    String headerContent = line.substring(line.indexOf(":") + 1);
    headerContent.trim();

    this->headers.insert(std::make_pair(headerName, headerContent));
  }
}

HTTPMethod HTTPRequest::getMethod() const
{
  return this->method;
}

String HTTPRequest::getRequestPath() const
{
  return this->requestPath;
}

String HTTPRequest::getHttpVersion() const
{
  return this->httpVersion;
}

const std::map<String, String> &HTTPRequest::getHeaders() const
{
  return this->headers;
}

const std::map<String, String> &HTTPRequest::getBody() const
{
  return this->body;
}

WiFiClient *HTTPRequest::getClientPtr() const
{
  return this->client;
}

HTTPMethod HTTPRequest::getMethodFromString(const String &methodString)
{
  if (methodString == "GET")
  {
    return HTTPMethod::GET;
  }
  else if (methodString == "POST")
  {
    return HTTPMethod::POST;
  }
  else if (methodString == "OPTIONS")
  {
    return HTTPMethod::OPTIONS;
  }
  else
  {
    return HTTPMethod::METHOD_NOT_ALLOWED;
  }
}

String HTTPRequest::getStringFromMethod(const HTTPMethod method)
{
  switch (method)
  {
  case HTTPMethod::GET:
    return "GET";
  case HTTPMethod::POST:
    return "POST";
  case HTTPMethod::OPTIONS:
    return "OPTIONS";
  default:
    return "ERORR";
  }
}
