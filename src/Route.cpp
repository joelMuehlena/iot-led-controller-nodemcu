#include "Route.h"

Route::Route(const String &path, HTTPMethod method, HttpHandlerFunc handler) : method(method), path(path), handler(handler)
{
}

void Route::runHandler(const HTTPRequest &request, HTTPResponse &response) const
{
  this->handler(request, response);
}

String Route::getPath() const
{
  return this->path;
}

HTTPMethod Route::getMethod() const
{
  return this->method;
}
