#include "WebSocket.h"

void WebSocket::webSocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length)
{
  if (type == WStype_TEXT)
  {
    String p = String(reinterpret_cast<const char *>(payload));
    p.trim();

    DynamicJsonDocument doc(2048);
    DeserializationError err = deserializeJson(doc, p);

    if (err.code() == DeserializationError::Ok)
    {
      String channel = doc["channel"];
      bool isBroadcast = doc["isBroadcast"];

      JsonVariant data_ref = doc["data"].as<JsonObject>();
      DynamicJsonDocument data = data_ref;

      if ((channel == "null" && !isBroadcast) || data.isNull())
      {
        this->server->sendTXT(num, "ERROR: Please provide a valid JSON Payload");
      }

      if (isBroadcast)
      {
        this->updateAll(num, data);
      }
      else
      {
        this->update(num, channel.c_str(), data);
      }
    }
    else
    {
      this->server->sendTXT(num, "ERROR: Please provide a valid JSON Payload");
    }
  }
  else if (type == WStype_CONNECTED)
  {
    // send message to client
    if (this->onConnectCB)
    {
      this->onConnectCB(std::bind(&WebSocket::sendText, this, num, std::placeholders::_2));
    }

    Serial.print(num);
    Serial.println(" WS connected");
  }
}

WebSocket::WebSocket(uint16_t port) : server(nullptr), isClosed(true), port(port)
{
  this->startServer();
}

WebSocket::WebSocket(WebSocket &&socket) : server(std::move(socket.server)), observers(std::move(socket.observers)), isClosed(std::exchange(socket.isClosed, true)), port(std::exchange(socket.port, 0))
{
  this->setCallback();
}

void WebSocket::startServer(bool clearObservers)
{
  if (port != 0)
  {
    Serial.println("WS started");
    this->server = std::make_unique<WebSocketsServer>(this->port);

    if (clearObservers)
      this->observers.clear();

    this->server->begin();
    this->setCallback();
    this->isClosed = false;
  }
}

void WebSocket::setPort(uint16_t port, bool clearObservers)
{
  this->port = port;
  this->startServer(clearObservers);
}

void WebSocket::setCallback()
{
  this->server->onEvent(std::bind(&WebSocket::webSocketEvent, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
}

WebSocket &WebSocket::operator=(WebSocket &&socket)
{
  if (this != &socket)
  {
    this->server = std::move(socket.server);
    this->setCallback();
    this->observers = std::move(socket.observers);
    this->isClosed = std::exchange(socket.isClosed, true);
    this->port = std::exchange(socket.port, 0);
  }

  return *this;
}

WebSocket::~WebSocket()
{
  if (!this->isClosed)
    this->server->close();
}

void WebSocket::onConnect(std::function<void(ReturnChannelCb)> cb)
{
  this->onConnectCB = cb;
}

uint32_t WebSocket::addObserver(const char *channel, wsCallback callback)
{
  auto it = this->observers.find(channel);

  if (it != this->observers.end())
  {
    it->second.push_back(callback);
  }
  else
  {
    auto pair = std::make_pair<const char *, std::list<wsCallback>>(std::move(channel), std::list<wsCallback>());
    pair.second.push_back(callback);
    this->observers.insert(pair);
  }

  uint32_t handle = 0;

  return handle;
}

bool WebSocket::removeObserver(const char *channel, const uint32_t &handle)
{
  auto it = this->observers.find(channel);
  if (it != this->observers.end())
  {
    // TODO remove by handle
  }

  return false;
}

uint32_t WebSocket::subscribe(const char *channel, wsCallback callback)
{
  return this->addObserver(channel, callback);
}

bool WebSocket::unsubscribe(const char *channel, const uint32_t handle)
{
  return this->removeObserver(channel, handle);
}

void WebSocket::update(uint8_t senderNum, const char *channel, DynamicJsonDocument data) const
{
  Serial.print("Update for channel: ");
  Serial.println(channel);
  auto it = this->observers.find(std::move(channel));

  if (it != this->observers.end())
  {
    Serial.print("Found channel: ");
    Serial.println(it->first);
    auto start = it->second.begin();
    auto end = it->second.end();

    for (auto i = start; i != end; ++i)
    {
      // TODO fix placeholder for return channel
      (*i)(data, std::bind(&WebSocket::sendText, this, senderNum, std::placeholders::_2));
    }
  }
}

void WebSocket::updateAll(uint8_t senderNum, DynamicJsonDocument data) const
{

  for (auto it = this->observers.begin(); it != this->observers.end(); ++it)
  {
    auto start = it->second.begin();
    auto end = it->second.end();

    for (auto i = start; i != end; ++i)
    {
      (*i)(data, std::bind(&WebSocket::sendText, this, senderNum, std::placeholders::_2));
    }
  }
}

void WebSocket::sendText(uint8_t num, const String &txt) const
{
  this->server->sendTXT(num, txt.c_str());
}

void WebSocket::broadcastText(const String &txt) const
{
  this->server->broadcastTXT(txt.c_str());
}

int WebSocket::getClientCount() const
{
  return this->server->connectedClients();
}

void WebSocket::broadcastText(const char *channel, const String &txt) const
{
  String s = "{\"channel\":\"" + String(channel) + "\",\"isBroadcast\":true,\"data\":" + txt + "}";
  this->server->broadcastTXT(s.c_str());
}

void WebSocket::close()
{
  this->server->close();
  this->isClosed = true;
}

void WebSocket::loop()
{
  if (this->server != nullptr && !this->isClosed)
  {
    this->server->loop();
  }
}
