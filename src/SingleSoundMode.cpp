#include "SingleSoundMode.h"
#include "Ardyt.h"

SingleSoundLEDMode::SingleSoundLEDMode() : LEDMode("SINGLESOUNDLEDMODE"), threshold(50), colorFrom(RGBColor(255, 186, 3)), colorTo(RGBColor(128, 23, 173)), last(0), avg(0), low(0), high(0), currVol(0), sensibility(1.0)
{
  RGBColor c = RGBColor(120, 120, 120);
  this->color = c;

  startNeoPixelFade();
  this->timer = millis();
  this->delayTimer = 0;

  Ardyt::getInstance()->getWS()->subscribe("singleSoundMode", std::bind(&SingleSoundLEDMode::onWebSocketEvent, this, std::placeholders::_1, std::placeholders::_2));
}

void SingleSoundLEDMode::onWebSocketEvent(DynamicJsonDocument data, WebSocket::ReturnChannelCb sendBack)
{
  String cmd = data["cmd"];
  if (cmd != "null")
  {
    cmd.toLowerCase();

    return;
  }

  if (!data["sensibility"].isNull() && data["sensibility"].is<float>())
  {
    float sens = data["sensibility"];
    this->setSensibility(sens);
  }
}

SingleSoundLEDMode::~SingleSoundLEDMode()
{
  Ardyt::getInstance()->getWS()->unsubscribe("singleSoundMode", 0);
}

void SingleSoundLEDMode::soundVisualize(int soundValue)
{
  this->currVol = soundValue;

  if (this->currVol < this->avg / 2.0 || this->currVol < 30)
  {
    this->currVol = 0;
  }
  else
  {
    this->avg = (this->avg + this->currVol) / 2.0;
  }

  if (this->currVol > this->high)
    this->high = this->currVol;

  if (this->currVol < this->low || this->low == 0)
    this->low = this->currVol;

  if (this->currVol < this->low)
    return;

  if (millis() - this->timer > 60 * 1000)
  {
    this->high = (this->high + this->currVol) / 2.0;
    this->timer = millis();
  }

  if (this->currVol - this->last > this->avg - this->last && this->avg - this->last > 0)
    this->avgBump = (this->avgBump + (this->currVol - this->last)) / 2.0;

  this->bump = (this->currVol - this->last) > (this->avgBump + ((float)this->threshold * this->sensibility));

  if (this->bump)
  {
    float pct = pow((this->currVol) / (this->high), 2.0) * 100.0;
    pct = pct > 100.0 ? 100.0 : pct;

    LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::BRIGHTNESS, pct);
    LEDMode::notify(msg);

    this->loopNeoPixel();

    this->delayTimer = millis();
  }
  else
  {
    if (millis() - this->delayTimer > 40)
    {
      LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, LEDController::offColor);
      LEDMode::notify(msg);
    }
  }

  this->last = this->currVol;
}

void SingleSoundLEDMode::handleMode()
{
#ifdef HAS_MIC
  soundVisualize(analogRead(MIC));
#else
  Serial.println("No mic available");
#endif
}

void SingleSoundLEDMode::handleModeExtern(int n)
{
  soundVisualize(n);
}

void SingleSoundLEDMode::setThreshold(int value)
{
  this->threshold = value;
}

void SingleSoundLEDMode::setSensibility(float sensibility)
{
  if (this->sensibility != sensibility)
  {
    this->sensibility = sensibility > 2.0 ? 2.0 : sensibility;
    this->recalcValues();
  }
}

float SingleSoundLEDMode::getSensibility() const
{
  return this->sensibility;
}

void SingleSoundLEDMode::setState(int *state)
{
  r0 = state[0];
  g0 = state[1];
  b0 = state[2];
  r1 = state[3];
  g1 = state[4];
  b1 = state[5];

  dx = state[6];
  dy = state[7];
  dz = state[8];
  sx = state[9];
  sy = state[10];
  sz = state[11];
  dm = state[12];
  steps = state[13];
  _isEnded = false;
}

int *SingleSoundLEDMode::getState() const
{
  int *state = new int[14];

  state[0] = r0;
  state[1] = g0;
  state[2] = b0;
  state[3] = r1;
  state[4] = g1;
  state[5] = b1;

  state[6] = dx;
  state[7] = dy;
  state[8] = dz;
  state[9] = sx;
  state[10] = sy;
  state[11] = sz;
  state[12] = dm;
  state[13] = steps;

  return state;
}

void SingleSoundLEDMode::recalcValues()
{
  this->high = (this->high * this->sensibility + this->currVol * this->sensibility) / 2.0;
  this->timer = millis();
  this->last = this->last * this->sensibility;
  this->avgBump = 0;
  this->avg = 0;
  this->low = this->low * this->sensibility;
}

void SingleSoundLEDMode::setColorFrom(RGBColor c)
{
  if (c != this->colorFrom)
  {
    this->colorFrom = c;
    startNeoPixelFade();
  }
}

void SingleSoundLEDMode::setColorTo(RGBColor c)
{
  if (c != this->colorTo)
  {
    this->colorTo = c;
    startNeoPixelFade();
  }
}

RGBColor SingleSoundLEDMode::getColorFrom() const
{
  return this->colorFrom;
}

RGBColor SingleSoundLEDMode::getColorTo() const
{
  return this->colorTo;
}

SingleSoundLEDMode::Direction SingleSoundLEDMode::getDirection() const
{
  return this->dir;
}

void SingleSoundLEDMode::setDirection(Direction dir)
{
  this->dir = dir;
}

void SingleSoundLEDMode::startNeoPixelFade()
{
  setPlotLine(this->colorFrom.r, this->colorFrom.g, this->colorFrom.b, this->colorTo.r, this->colorTo.g, this->colorTo.b);
  this->dir = Direction::FORWARDS;
}

void SingleSoundLEDMode::loopNeoPixel()
{
  LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, RGBColor(r0, g0, b0));
  LEDMode::notify(msg);

  NeoPixel::_isEnded = (NeoPixel::steps == 0);
  if (!_isEnded)
  {
    --steps;
    r1 -= dx;
    g1 -= dy;
    b1 -= dz;

    if (r1 < 0)
    {
      r1 += dm;
      r0 += sx;
    }

    if (g1 < 0)
    {
      g1 += dm;
      g0 += sy;
    }

    if (b1 < 0)
    {
      b1 += dm;
      b0 += sz;
    }
  }
  else
  {
    if (this->dir == Direction::BACKWARDS)
    {
      setPlotLine(this->colorFrom.r, this->colorFrom.g, this->colorFrom.b, this->colorTo.r, this->colorTo.g, this->colorTo.b);
      this->dir = Direction::FORWARDS;
    }
    else
    {
      setPlotLine(this->colorTo.r, this->colorTo.g, this->colorTo.b, this->colorFrom.r, this->colorFrom.g, this->colorFrom.b);
      this->dir = Direction::BACKWARDS;
    }
  }
}

void SingleSoundLEDMode::configMode(DynamicJsonDocument &configData)
{
}