#include "DiscoveryService.h"

const String DiscoveryService::DISCOVERY_MODE = "DISCOVERY";
const String DiscoveryService::DISCOVERED_MODE = "DISCOVERED";
const String DiscoveryService::PROTOCOL_HEADER = "JM-IOT-DEVICE PROTOCOL 1.0\n";

DiscoveryService::DiscoveryService(uint16_t port) : port(port)
{
  udp.begin(this->port);
}

void DiscoveryService::loop()
{

  uint16_t packetSize = udp.parsePacket();
  if (packetSize)
  {
    char packetBuffer[255];
    udp.read(packetBuffer, sizeof(packetBuffer));
    packetBuffer[packetSize] = 0;
    String s = String(packetBuffer);
    if (s.startsWith(DiscoveryService::PROTOCOL_HEADER))
    {
      int newLine = s.indexOf("\n");
      int nextNewLine = s.indexOf("\n", newLine + 1);
      Serial.println("UDP of Device Protocol received: ");
      Serial.println(s);

      String method = s.substring(newLine + 1, nextNewLine);
      s = s.substring(nextNewLine + 1, s.indexOf("\n", nextNewLine + 1));

      if (method == DiscoveryService::DISCOVERY_MODE)
      {
        this->checkHasDiscovered(udp.remoteIP(), udp.remotePort(), s);
      }
    }
  }
}

void DiscoveryService::checkHasDiscovered(IPAddress ip, uint16_t port, const String &packet)
{
  Serial.println("is discovery");
  Serial.println(packet);
  udp.beginPacket(ip, port);
  udp.print(DiscoveryService::PROTOCOL_HEADER);
  udp.print(DiscoveryService::DISCOVERED_MODE + "\n");
  udp.print("X-Discovered-IP:" + WiFi.localIP().toString() + " \n");
  udp.print("X-Discovered-MAC:" + WiFi.macAddress() + " \n");
  udp.endPacket();
}