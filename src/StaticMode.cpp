#include "StaticMode.h"

#include "LEDController.h"

static LEDController *controller = LEDController::getInstance();

StaticLEDMode::StaticLEDMode() : LEDMode("STATICLEDMODE"), color(controller->getColor()) {}

void StaticLEDMode::handleMode()
{
  if (this->color != controller->getColor())
  {
    LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, this->color);
    LEDMode::notify(msg);
  }
}

void StaticLEDMode::configMode(DynamicJsonDocument &configData)
{

  String color = configData["color"];

  if (color != "null")
  {
    color.replace("#", "");
    RGBColor c = util::hexToRgb(color);
    this->setColor(c);
  }
}

void StaticLEDMode::setColor(RGBColor c)
{
  this->color = c;

  LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, c);
  LEDMode::notify(msg);
}