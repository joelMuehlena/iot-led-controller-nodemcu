#include "LEDController.h"
#include <Arduino.h>
#include "LedModeHelper.h"
#include "Ardyt.h"

LEDController *LEDController::instance = nullptr;
RGBColor LEDController::offColor = RGBColor(0);
const String LEDController::UUIDESPCONTROLLER = "esp-led-" + WiFi.macAddress();

LEDController::LEDController()
{
  RGBColor defaultColor;
  defaultColor.r = 4;
  defaultColor.g = 246;
  defaultColor.b = 206;

  this->color = defaultColor;
  this->brightness = 1.0f;
  this->state = State::ON;
}

LEDController *LEDController::getInstance()
{
  if (LEDController::instance == nullptr)
  {
    LEDController::instance = new LEDController();
  }

  return LEDController::instance;
}

void LEDController::trigger()
{
  if (this->currentMode != nullptr && this->state == State::ON)
  {
    this->currentMode->handleMode();
  }
}

RGBColor LEDController::getColor() const
{
  return this->color;
}

void LEDController::setAllPins(const uint8_t rPin, const uint8_t bPin, const uint8_t gPin)
{
  this->redPin = rPin;
  this->bluePin = bPin;
  this->greenPin = gPin;
}

void LEDController::Init()
{
  this->currentMode = std::make_shared<StaticLEDMode>();
  this->currentMode->Attach(this);
  analogWrite(this->redPin, this->color.r * this->brightness);
  analogWrite(this->greenPin, this->color.g * this->brightness);
  analogWrite(this->bluePin, this->color.b * this->brightness);
}

void LEDController::setBrightness(int value)
{
  this->brightness = (float)value / 100.0f > 1.0 ? 1.0 : (float)value / 100.0f;

  analogWrite(this->redPin, this->color.r * this->brightness);
  analogWrite(this->greenPin, this->color.g * this->brightness);
  analogWrite(this->bluePin, this->color.b * this->brightness);
}

void LEDController::setColor(RGBColor c)
{
  this->color = c;
  analogWrite(this->redPin, this->color.r * this->brightness);
  analogWrite(this->greenPin, this->color.g * this->brightness);
  analogWrite(this->bluePin, this->color.b * this->brightness);
}

void LEDController::setMode(std::shared_ptr<LEDMode> newLedMode)
{
  this->currentMode = newLedMode;
}

std::shared_ptr<LEDMode> LEDController::getLEDMode()
{
  return this->currentMode;
}

void LEDController::Update(LEDModeUpdateMSG msg)
{
  switch (msg.updateType)
  {
  case LEDModeUpdateMSG::UpdateType::COLOR:
    this->setColor(msg.color);
    this->writeCurrentConfig();
    break;
  case LEDModeUpdateMSG::UpdateType::BRIGHTNESS:
    this->setBrightness(msg.brightness);
    this->writeCurrentConfig();
    break;
  case LEDModeUpdateMSG::UpdateType::MODE:
    if (msg.newLedMode != nullptr)
    {
      this->setMode(msg.newLedMode);
      this->writeCurrentConfig(true);
    }
    break;
  }
}

String LEDController::createConnectConfig() const
{
  DynamicJsonDocument doc(2048);
  doc["channel"] = "config";
  doc["isBroadcast"] = false;
  JsonObject dataPart = doc.createNestedObject("data");
  JsonObject initConfPart = dataPart.createNestedObject("initialConfig");
  initConfPart["brightness"] = this->brightness * 100;
  initConfPart["currentMode"] = this->currentMode->getName();
  initConfPart["currentColor"] = this->color.toString();
  initConfPart["controllerState"] = this->state == State::ON ? "on" : "off";

  String ret = "";
  serializeJson(doc, ret);
  return ret;
}

void LEDController::writeCurrentConfig(bool skipConfigInterval)
{

  if (!this->isConfigWriteEnabled)
    return;

  if ((millis() - this->configWriteIntervalTimer > this->configWriteInterval) || skipConfigInterval)
  {
    DynamicJsonDocument configDoc(1024);
    bool configFile = LittleFSWrapper::readJSONFile(configDoc, LittleFS, "/config.json");

    if (configFile)
    {
      JsonObject controllerConfig = configDoc.createNestedObject("controllerConfig");

      controllerConfig["brightness"] = this->brightness * 100;
      controllerConfig["currentMode"] = this->currentMode->getName();
      controllerConfig["currentColor"] = this->color.toString();
      controllerConfig["controllerName"] = this->controllerName;
      controllerConfig["controllerState"] = this->state == LEDController::State::ON ? "on" : "off";

      LittleFSWrapper::writeJSONFile(configDoc, LittleFS, "/config.json");
    }
    this->configWriteIntervalTimer = millis();
  }
}

LEDController::State LEDController::getState() const
{
  return this->state;
}

void LEDController::setState(LEDController::State state)
{
  if (state == this->state)
    return;

  this->state = state;

  if (this->state == LEDController::State::ON)
    this->powerOn();
  else
    this->powerOff();
}

void LEDController::powerOff()
{
  LOG::print("Power off", "tst", 5, false);

  this->writeCurrentConfig(true);

  // No real switch yet
  this->setColor(LEDController::offColor);
  Ardyt::getInstance()->getWS()->broadcastText("{\"channel\":\"config\",\"isBroadcast\":true,\"data\":{\"controllerState\":\"off\"}}");
}

void LEDController::powerOn()
{
  Serial.println("Power on");
  this->isConfigWriteEnabled = false;
  DynamicJsonDocument configDoc(2048);
  bool configFile = LittleFSWrapper::readJSONFile(configDoc, LittleFS, "/config.json");
  if (configFile)
  {
    JsonVariant controllerConfig = configDoc["controllerConfig"];
    if (!controllerConfig.isNull() && controllerConfig.is<JsonObject>())
    {
      String brightness = controllerConfig["brightness"];
      const char *currentMode = controllerConfig["currentMode"];
      String currentColor = controllerConfig["currentColor"];

      if (currentMode != "null")
      {
        LEDModeHelper::changeLEDMode(currentMode);
      }

      if (currentColor != "null")
      {
        LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, util::hexToRgb(currentColor));
        LEDController::getInstance()->getLEDMode()->notify(msg);
      }

      if (brightness != "null")
      {
        LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::BRIGHTNESS, brightness.toInt());
        LEDController::getInstance()->getLEDMode()->notify(msg);
      }
    }
  }

  this->isConfigWriteEnabled = true;
  this->writeCurrentConfig(true);

  Ardyt::getInstance()->getWS()->broadcastText("{\"channel\":\"config\",\"isBroadcast\":true,\"data\":{\"controllerState\":\"on\"}}");
  Ardyt::getInstance()->getWS()->broadcastText(LEDController::getInstance()->createConnectConfig());
  Serial.println("End of power on");
}

String LEDController::getName() const
{
  return this->controllerName;
}

void LEDController::setName(String name)
{

  if (name == "")
  {
    name = "deault-esp-controller-name";
  }

  this->controllerName = name;
  this->writeCurrentConfig(true);
}
