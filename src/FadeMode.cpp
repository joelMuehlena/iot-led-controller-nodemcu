#include "FadeMode.h"

#include "LEDController.h"

FadeLEDMode::FadeLEDMode() : LEDMode("FADELEDMODE"), NeoPixel(), colorFrom(RGBColor(255, 186, 3)), colorTo(RGBColor(128, 23, 173)), dir(Direction::FORWARDS)
{
  startNeoPixelFade();
  this->setPeriod(1000 * 6);
}

void FadeLEDMode::handleMode()
{
  this->loopNeoPixel();
}

void FadeLEDMode::setColorFrom(RGBColor c)
{

  if (c == this->colorTo || c == this->colorFrom)
    return;

  this->colorFrom = c;
  startNeoPixelFade();
}
void FadeLEDMode::setColorTo(RGBColor c)
{
  if (c == this->colorFrom || c == this->colorTo)
    return;

  this->colorTo = c;
  startNeoPixelFade();
}

void FadeLEDMode::startNeoPixelFade()
{
  setPlotLine(this->colorFrom.r, this->colorFrom.g, this->colorFrom.b, this->colorTo.r, this->colorTo.g, this->colorTo.b);
  this->dir = Direction::FORWARDS;
}

void FadeLEDMode::loopNeoPixel()
{
  if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - this->lastTime).count() < this->interval)
  {
    return;
  }

  this->lastTime = std::chrono::system_clock::now();
  LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, RGBColor(r0, g0, b0));
  LEDMode::notify(msg);

  NeoPixel::_isEnded = (NeoPixel::steps == 0);
  if (!_isEnded)
  {
    --steps;
    r1 -= dx;
    g1 -= dy;
    b1 -= dz;

    if (r1 < 0)
    {
      r1 += dm;
      r0 += sx;
    }

    if (g1 < 0)
    {
      g1 += dm;
      g0 += sy;
    }

    if (b1 < 0)
    {
      b1 += dm;
      b0 += sz;
    }
  }
  else
  {
    if (this->dir == Direction::BACKWARDS)
    {
      setPlotLine(this->colorFrom.r, this->colorFrom.g, this->colorFrom.b, this->colorTo.r, this->colorTo.g, this->colorTo.b);
      this->dir = Direction::FORWARDS;
    }
    else
    {
      setPlotLine(this->colorTo.r, this->colorTo.g, this->colorTo.b, this->colorFrom.r, this->colorFrom.g, this->colorFrom.b);
      this->dir = Direction::BACKWARDS;
    }
  }
}

void FadeLEDMode::configMode(DynamicJsonDocument &configData)
{

  String colorFrom = configData["fromColor"];
  String colorTo = configData["toColor"];
  String period = configData["period"];

  if (colorFrom != "null")
  {
    colorFrom.replace("#", "");
    RGBColor c = util::hexToRgb(colorFrom);
    this->setColorFrom(c);
  }

  if (colorTo != "null")
  {
    colorTo.replace("#", "");
    RGBColor c = util::hexToRgb(colorTo);
    this->setColorTo(c);
  }

  if (period != "null")
  {
    int p = period.toInt();
    setPeriod(p * 1000);
  }
}
