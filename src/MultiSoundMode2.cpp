#include "MultiSoundMode2.h"
#include "Ardyt.h"

#define DBG(x)                       \
  Serial.print("[MultiSoundMode] "); \
  Serial.println(x);

MultiSoundLEDMode::MultiSoundLEDMode(Type type) : LEDMode("MULTISOUNDLEDMODE")
{
}

MultiSoundLEDMode::~MultiSoundLEDMode() {}

void MultiSoundLEDMode::handleMode() {}

void MultiSoundLEDMode::configMode(DynamicJsonDocument &configData) {}