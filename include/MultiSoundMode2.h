#ifndef LED_MODE_MULTI_SOUND_HEADER_2
#define LED_MODE_MULTI_SOUND_HEADER_2

#include <functional>
#include <array>

#include <WebSocketsServer.h>
#include <WebSocketsClient.h>
#include <WiFiUdp.h>

#include "WebSocket.h"

#include "LEDModeIncludes.h"

#include "LEDController.h"

// Just a ip address full of zeros used in this context as not set
static const IPAddress zeroAddress(0, 0, 0, 0);

// Forward declaration
class SingleSoundLEDMode;

/**
 * @brief A LEDMode for syninc multiple controllers to a single sound source
 * @details This LEDMode can sync the LEDState of c multiple controllers to a single sound source.
 * It can act as a server (the part which shares the state), a client (the part which receives the state) or a bridge (a client which works as a repeater will be used if the WebSocket capacity of a server is maxed out).
 * The server gets all initial requests and asks a client to change to bridge mode if required.
 *
 */
class MultiSoundLEDMode : public LEDMode
{

public:
  // Types which a MultiSoundMode can be
  enum class Type
  {
    /// Main server which distributes its state to the bridges and clients
    SERVER,
    /// Just receives the state of a server and applies it. Can switch to a bridge
    CLIENT
  };

private:
public:
  /**
   * @brief Construct a new MultiSoundLEDMode object
   *
   * @param type The type in which the Mode will start. Defaults to client.
   */
  MultiSoundLEDMode(Type type = Type::CLIENT);
  ~MultiSoundLEDMode();

  /**
   * @brief Overrides the handleMode and handles the loop of the mode
   * @details Gets called periodically and handles the websocket and discover connections
   */
  void handleMode() override;

  void configMode(DynamicJsonDocument &configData) override;
};

#endif