#ifndef HTTPREQUEST_HEADER
#define HTTPREQUEST_HEADER

#include <WiFi.h>

#include <map>

enum class HTTPMethod
{
  GET,
  POST,
  OPTIONS,
  METHOD_NOT_ALLOWED
};

class HTTPRequest
{

private:
  WiFiClient *const client;
  std::map<String, String> headers;
  HTTPMethod method;
  String requestPath;
  String httpVersion;
  String contentString;
  std::map<String, String> body;

  boolean hasError = false;

  void parseHeaders();
  void parseRequest();

  void parseBody();

public:
  static void urldecode(String &string);

  HTTPRequest() = delete;
  HTTPRequest(WiFiClient *const client);

  static HTTPMethod getMethodFromString(const String &string);
  static String getStringFromMethod(const HTTPMethod method);

  HTTPMethod getMethod() const;
  String getRequestPath() const;
  String getHttpVersion() const;
  WiFiClient *getClientPtr() const;
  const std::map<String, String> &getHeaders() const;
  const std::map<String, String> &getBody() const;
};

#endif
