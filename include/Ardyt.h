#ifndef ARDYT_HEADER
#define ARDYT_HEADER

#include <WiFi.h>
#include <PubSubClient.h>

#include "Route.h"
#include "WebSocket.h"

#include <vector>

#include <memory>
#include <functional>

#include "DiscoveryService.h"

extern bool CONFIGMODE;

class Ardyt
{
private:
  String ssid;
  String password;

  static std::shared_ptr<Ardyt> instance;

  Ardyt();

  bool isServerrunning = false;
  bool isCorsEnabled = true;
  bool isConfigMode = false;

  unsigned long previousMillis = 0;
  unsigned long interval = 30 * 1000;

  int port = -1;
  WiFiServer server;

  // WebSocket
  uint16_t wsPort = -1;
  WebSocket webSocket;

  // Discovery Protocol
  DiscoveryService discoveryService;

  // MQTT
  WiFiClient wifiClient;
  PubSubClient mqttClient;
  String specificChannelName;

  std::vector<std::shared_ptr<Route>> routes;

  void connectToWifi();
  void startServer();

  void logRequest(const HTTPRequest &request);

public:
  using HttpHandlerFunc = Route::HttpHandlerFunc;

  Ardyt(const Ardyt &) = delete;
  Ardyt &operator=(const Ardyt &) = delete;
  Ardyt(Ardyt &&) = delete;
  Ardyt &operator=(Ardyt &&) = delete;

  void Run(bool configMode = false);

  void Get(String path, HttpHandlerFunc method);
  void Post(String path, HttpHandlerFunc method);

  // void Use(void (*method)(const HTTPRequest &request, HTTPResponse &response));

  void handleRequest(const HTTPRequest &request);
  void endRequest();

  WiFiClient available();

  // WebSocket
  void setWSPort(uint16_t port);
  uint16_t getWSPort();
  WebSocket *getWS();

  // MQTT
  PubSubClient &getMQTTClient();
  bool reconnectMQTT();
  void addMQTTCallback(void (*callback)(char *topic, byte *payload, unsigned int length));
  const String &getMQTTSpecificName() const;

  void setPort(int port);
  void setWiFiCredentials(String ssid, String password);
  String getWiFiSSID() const;
  String getWiFiPass() const;

  static std::shared_ptr<Ardyt> getInstance();

  void loop();
  void loop(bool loopWs, bool loopMQTT, bool handleHttp);
};

#endif
