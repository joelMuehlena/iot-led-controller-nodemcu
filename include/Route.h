#ifndef ROUTE_HEADER
#define ROUTE_HEADER

#include "HTTPRequest.h"
#include "HTTPResponse.h"

class Route
{
public:
  using HttpHandlerFunc = std::function<void(const HTTPRequest &, HTTPResponse &)>;

private:
  HTTPMethod method;
  String path;
  HttpHandlerFunc handler;

public:
  Route() = delete;
  Route(const String &path, HTTPMethod method, HttpHandlerFunc handler);

  String getPath() const;
  HTTPMethod getMethod() const;
  void runHandler(const HTTPRequest &request, HTTPResponse &response) const;
};

#endif