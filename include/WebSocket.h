#ifndef WEBSOCKET_WRAPPER
#define WEBSOCKET_WRAPPER

#include <WebSocketsServer.h>
#include <ArduinoJson.h>
#include <list>
#include <map>
#include <functional>
#include <memory>
#include <utility>
#include <cstring>

class WebSocket
{
public:
  using ReturnChannelCb = std::function<void(uint8_t, const String &)>;
  using wsCallback = std::function<void(DynamicJsonDocument, ReturnChannelCb)>;
  typedef uint8_t unused_uint8_t;

private:
  // TODO implement for unregister (needs unique id per cb)
  struct cb_storage_t
  {
    wsCallback cb;
    uint32_t handle;
  };

  struct cmp_str
  {
    bool operator()(char const *a, char const *b) const
    {
      return std::strcmp(a, b) < 0;
    }
  };

  std::unique_ptr<WebSocketsServer>
      server;
  std::map<const char *, std::list<wsCallback>, cmp_str> observers;
  bool isClosed;
  uint16_t port;

  std::function<void(ReturnChannelCb)> onConnectCB;

  uint32_t addObserver(const char *channel, wsCallback callback);
  bool removeObserver(const char *channel, const uint32_t &handle);
  void setCallback();

  void startServer(bool clearObservers = true);

public:
  WebSocket() = delete;
  WebSocket(WebSocket &socket) = delete;

  explicit WebSocket(uint16_t port);
  WebSocket(WebSocket &&socket);

  WebSocket &operator=(WebSocket &&socket);
  WebSocket &operator=(const WebSocket &socket) = delete;

  void sendText(uint8_t num, const String &txt) const;
  void broadcastText(const String &txt) const;
  void broadcastText(const char *channel, const String &txt) const;

  ~WebSocket();

  void webSocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length);

  uint32_t subscribe(const char *channel, wsCallback callback);
  bool unsubscribe(const char *channel, const uint32_t handle);
  void update(uint8_t senderNum, const char *channel, DynamicJsonDocument data) const;
  void updateAll(uint8_t senderNum, DynamicJsonDocument data) const;

  void onConnect(std::function<void(ReturnChannelCb)> cb);

  int getClientCount() const;

  void setPort(uint16_t port, bool clearObservers = false);

  void loop();
  void close();
};

#endif