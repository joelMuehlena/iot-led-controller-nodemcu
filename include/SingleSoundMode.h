#ifndef LED_MODE_SINGLE_SOUND_HEADER
#define LED_MODE_SINGLE_SOUND_HEADER

#include "LEDModeIncludes.h"

#include "LEDController.h"
#include <random>
#include <chrono>
#include <functional>

#include "NeoPixel.h"
class SingleSoundLEDMode : public LEDMode, public NeoPixel
{
public:
  enum class Direction
  {
    FORWARDS,
    BACKWARDS
  };

private:
  int threshold;

  // Fade colors & NeoPixel vars
  RGBColor colorFrom;
  RGBColor colorTo;
  Direction dir;

  RGBColor color;
  float last;
  float avg;
  float avgBump;
  bool bump;
  float low;
  float high;
  float currVol;

  // intern timers
  size_t timer;
  size_t delayTimer;

  float sensibility;
  void recalcValues();

  void soundVisualize(int soundValue);
  void onWebSocketEvent(DynamicJsonDocument data, WebSocket::ReturnChannelCb sendBack) override;

public:
  SingleSoundLEDMode();
  ~SingleSoundLEDMode();
  void handleMode() override;
  void configMode(DynamicJsonDocument &configData) override;
  void handleModeExtern(int n);

  void startNeoPixelFade();

  void loopNeoPixel() override;

  float getSensibility() const;
  void setSensibility(float sensibility);

  void setThreshold(int value);

  RGBColor getColorFrom() const;
  RGBColor getColorTo() const;
  void setColorFrom(RGBColor c);
  void setColorTo(RGBColor c);

  Direction getDirection() const;
  void setDirection(Direction dir);

  int *getState() const;
  void setState(int *state);
};

#endif
