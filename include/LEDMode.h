#ifndef LED_MODE_HEADER
#define LED_MODE_HEADER

// class LEDController
#include <memory>

#include <Arduino.h>
#include <type_traits>
#include <list>

#include <ArduinoJson.h>
#include "WebSocket.h"
#include "RGBColor.h"

class LEDMode;

struct LEDModeUpdateMSG
{
  enum class UpdateType
  {
    COLOR,
    BRIGHTNESS,
    MODE
  };

  UpdateType updateType;
  RGBColor color;
  int brightness;
  std::shared_ptr<LEDMode> newLedMode;

  inline explicit LEDModeUpdateMSG(UpdateType type, RGBColor c) : updateType(type), color(c), newLedMode(nullptr) {}
  inline explicit LEDModeUpdateMSG(UpdateType type, int brightness) : updateType(type), brightness(brightness), newLedMode(nullptr) {}
  inline explicit LEDModeUpdateMSG(UpdateType type, std::shared_ptr<LEDMode> mode) : updateType(type), newLedMode(mode) {}
};

class LEDModeObserver
{
public:
  inline virtual ~LEDModeObserver(){};
  virtual void Update(LEDModeUpdateMSG) = 0;
};

class LEDModeObservable
{
public:
  inline virtual ~LEDModeObservable(){};
  virtual void Attach(LEDModeObserver *observer) = 0;
  virtual void Detach(LEDModeObserver *observer) = 0;
  virtual void notify(LEDModeUpdateMSG &msg) const = 0;
};

class LEDMode : public LEDModeObservable
{
private:
  /// The name of the LEDMode
  String modeName;
  std::list<LEDModeObserver *> list_observer_;

protected:
  virtual void onWebSocketEvent(DynamicJsonDocument data, WebSocket::ReturnChannelCb sendBack);

public:
  /**
   * @brief Construct a new LEDMode object
   *
   * @param name The name for the LEDMode
   */
  LEDMode(const String &name);

  /**
   * @brief Get the name of the LEDMode
   *
   * @return String The name of the LEDMode
   */
  String getName() const;

  /**
   * @brief Handles the loop of an LEDMode.
   * @details This method is called in the Arduino loop function. It must be overriden by every child of this class.
   * It should containt the logic which is meant to run repeatable.
   *
   */
  virtual void handleMode() = 0;
  virtual void configMode(DynamicJsonDocument &configData) = 0;

  void Attach(LEDModeObserver *observer) override;
  void Detach(LEDModeObserver *observer) override;
  void notify(LEDModeUpdateMSG &msg) const override;

  /**
   * @brief Casts a generic LEDMode to an specific one
   *
   * @tparam T The LEDMode child to cast to
   * @param ledMode The instance of the generic LEDMode
   * @return T Returns a pointer of type T or nullptr if the cast was not successful
   */
  template <class T, std::enable_if_t<std::is_pointer<T>::value, bool> = true>
  static T dynamicCast(LEDMode *const ledMode);
};

inline LEDMode::LEDMode(const String &name) : modeName(name) {}

inline String LEDMode::getName() const
{
  return this->modeName;
}

template <class T, std::enable_if_t<std::is_pointer<T>::value, bool> = true>
inline T LEDMode::dynamicCast(LEDMode *const ledMode)
{
  typedef typename std::remove_pointer<typename std::remove_cv<T>::type>::type typeNoConstNoPointer;

  if (std::is_class<typeNoConstNoPointer>::value && std::is_base_of<LEDMode, typeNoConstNoPointer>::value && std::is_polymorphic<typeNoConstNoPointer>::value)
  {
    T el = static_cast<T>(ledMode);
    return el;
  }
  else
  {
    return nullptr;
  }
}

inline void LEDMode::Attach(LEDModeObserver *observer) { this->list_observer_.push_back(observer); }
inline void LEDMode::Detach(LEDModeObserver *observer) { this->list_observer_.remove(observer); }
inline void LEDMode::notify(LEDModeUpdateMSG &msg) const
{

  if (this->list_observer_.size() == 0)
  {
    return;
  }

  auto it = this->list_observer_.begin();

  while (it != this->list_observer_.end())
  {
    (*it)->Update(msg);
    ++it;
  }
}

inline void LEDMode::onWebSocketEvent(DynamicJsonDocument data, WebSocket::ReturnChannelCb sendBack) {}

#endif
