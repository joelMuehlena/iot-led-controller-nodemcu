#include <ArduinoJson.h>

void singleSoundOptions(const HTTPRequest &request, HTTPResponse &response)
{
  if (LEDController::getInstance()->getLEDMode()->getName() != "SINGLESOUNDLEDMODE")
  {
    return response.setStatus(400)->send("ERROR THIS MODE IS NOT ACTIVE");
  }

  const auto body = request.getBody();

  auto bodySensibility = body.find("sensibility");

  if (bodySensibility == body.end())
  {
    DynamicJsonDocument doc(2048);
    doc["code"] = 400;
    doc["msg"] = "There was an error updating the SoundMode";

    JsonObject error = doc.createNestedObject("error");
    error["msg"] = "Please provide the argument sensibility";
    response.setStatus(400)->json(doc);
    return;
  }

  auto mode = LEDMode::dynamicCast<SingleSoundLEDMode *const>(LEDController::getInstance()->getLEDMode().get());

  if (bodySensibility != body.end())
  {
    mode->setSensibility(bodySensibility->second.toFloat());
  }

  response.setStatus(200)->send("SUCCESS");
}

void multiSoundOptions(const HTTPRequest &request, HTTPResponse &response)
{
  if (LEDController::getInstance()->getLEDMode()->getName() != "MULTISOUNDLEDMODE")
  {
    return response.setStatus(400)->send("ERROR THIS MODE IS NOT ACTIVE");
  }

  const auto body = request.getBody();

  auto bodySensibility = body.find("sensibility");
  auto bodyMode = body.find("mode");

  if (bodySensibility == body.end() && bodyMode == body.end())
  {
    DynamicJsonDocument doc(2048);
    doc["code"] = 400;
    doc["msg"] = "There was an error updating the SoundMode";

    JsonObject error = doc.createNestedObject("error");
    error["msg"] = "Please provide the argument sensibility or mode";
    response.setStatus(400)->json(doc);
    return;
  }

  auto mode = LEDMode::dynamicCast<MultiSoundLEDMode *const>(LEDController::getInstance()->getLEDMode().get());

  // if (bodySensibility != body.end())
  // {
  //   mode->getSoundMode()->setSensibility(bodySensibility->second.toFloat());
  // }

  // if (bodyMode != body.end())
  // {
  //   const_cast<String &>(bodyMode->second).toUpperCase();
  //   mode->setType(bodyMode->second == "SERVER" ? MultiSoundLEDMode::Type::SERVER : MultiSoundLEDMode::Type::CLIENT);
  // }

  response.setStatus(200)->send("SUCCESS");
}