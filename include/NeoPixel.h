#ifndef NEO_PIXEL_HEADER
#define NEO_PIXEL_HEADER

#include <chrono>
#include "util.h"

class NeoPixel
{
protected:
  int r0, g0, b0, r1, g1, b1;
  int dx, dy, dz, sx, sy, sz, dm, steps;

  bool _isEnded;

  std::chrono::system_clock::time_point lastTime;
  unsigned long interval;
  unsigned long period;

  void calcTimeInterval();

public:
  void setPlotLine(int r0, int g0, int b0, int r1, int g1, int b1);
  bool isEnded() const;
  void reset();
  void setPeriod(unsigned long period);
  virtual void loopNeoPixel() = 0;
};

inline void NeoPixel::setPlotLine(int r0, int g0, int b0, int r1, int g1, int b1)
{

  this->r0 = r0;
  this->g0 = g0;
  this->b0 = b0;
  this->r1 = r1;
  this->g1 = g1;
  this->b1 = b1;

  dx = util::abs(r1, r0);
  dy = util::abs(g1, g0);
  dz = util::abs(b1, b0);

  sx = r0 < r1 ? 1 : -1;
  sy = g0 < g1 ? 1 : -1;
  sz = b0 < b1 ? 1 : -1;
  dm = util::max3(dx, dy, dz);
  steps = dm;
  calcTimeInterval();

  this->_isEnded = false;
  this->lastTime = std::chrono::system_clock::now();
}

inline void NeoPixel::calcTimeInterval()
{
  if (this->steps == 0)
  {
    this->steps = 1;
  }

  this->interval = this->period / this->steps;

  if (this->interval == 0)
    this->interval = 1;
}

inline void NeoPixel::setPeriod(unsigned long period)
{
  this->period = period;
  this->calcTimeInterval();
}

inline void NeoPixel::reset()
{
  setPlotLine(r0, g0, b0, r1, g1, b1);
}

inline bool NeoPixel::isEnded() const
{
  return this->_isEnded;
}

#endif