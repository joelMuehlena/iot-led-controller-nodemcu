#include "defines.h"

#ifndef LED_MODE_HEADER
#include "LEDMode.h"
#endif

#ifndef LED_MODE_STATIC_HEADER
#include "StaticMode.h"
#endif

#ifndef LED_MODE_FADE_HEADER
#include "FadeMode.h"
#endif

#ifndef LED_MODE_SINGLE_SOUND_HEADER
#include "SingleSoundMode.h"
#endif

#ifndef LED_MODE_MULTI_SOUND_HEADER_2
#include "MultiSoundMode2.h"
#endif
