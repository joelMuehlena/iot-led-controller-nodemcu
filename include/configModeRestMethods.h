#include <WiFi.h>

void getSSIDS(const HTTPRequest &request, HTTPResponse &response)
{
  DynamicJsonDocument doc(4096);
  int networkSSIDS = WiFi.scanNetworks();
  if (networkSSIDS > 0)
  {
    doc["ssidCount"] = networkSSIDS;
    for (int i = 0; i < networkSSIDS; ++i)
    {
      Serial.println(WiFi.SSID(i));
      doc["ssids"][i] = WiFi.SSID(i);
    }
  }
  else
  {
    doc["ssidCount"] = 0;
  }

  response.setStatus(200)->json(doc);
}

void postCredentials(const HTTPRequest &request, HTTPResponse &response)
{
  auto body = request.getBody();
  auto bodySSID = body.find("ssid");
  auto bodyPass = body.find("password");
  if (bodySSID == body.end() || bodyPass == body.end())
  {
    response.setStatus(400)->send("ERROR");
    return;
  }

  Ardyt::getInstance()->setWiFiCredentials(bodySSID->second, bodyPass->second);

  response.setStatus(200)->send("SUCCESS");
  WiFi.begin(bodySSID->second.c_str(), bodyPass->second.c_str());
  WiFi.waitForConnectResult();
}

void getWiFiStateAndSaveConfig(const HTTPRequest &request, HTTPResponse &response)
{
  DynamicJsonDocument doc(512);

  if (WiFi.status() == WL_CONNECTED)
  {
    doc["code"] = 0;
    doc["success"] = true;

    response.setStatus(200)->json(doc);

    delay(100);

    DynamicJsonDocument creds(512);
    creds["wifiSSID"] = Ardyt::getInstance()->getWiFiSSID();
    creds["wifiPassword"] = Ardyt::getInstance()->getWiFiPass();

    LittleFSWrapper::writeJSONFile(creds, LittleFS, "/config.json");

    delay(500);

    ESP.restart();
  }
  else
  {
    String status;
    if (WiFi.status() == WL_NO_SSID_AVAIL)
    {
      status = "SSID not valid";
    }
    else
    {
      status = "WiFi ERROR";
    }

    doc["code"] = WiFi.status();
    doc["success"] = false;

    doc["msg"] = status;
    response.setStatus(400)->json(doc);
  }
}