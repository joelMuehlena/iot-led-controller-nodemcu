#ifndef DISCOVERY_SERVICE_H
#define DISCOVERY_SERVICE_H

#include <WiFi.h>
#include <WiFiUdp.h>

class DiscoveryService
{

private:
  uint16_t port;
  WiFiUDP udp;

  void checkHasDiscovered(IPAddress ip, uint16_t port, const String &packet);

public:
  static const String DISCOVERY_MODE;
  static const String DISCOVERED_MODE;
  static const String PROTOCOL_HEADER;

  DiscoveryService(uint16_t port);

  void loop();
};

#endif