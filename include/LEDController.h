#ifndef LED_CONTROLLER_HEADER
#define LED_CONTROLLER_HEADER

#include <cstdint>
#include <memory>

#ifndef RGB_COLOR_HEADER
#include "RGBColor.h"
#endif

#ifndef LED_MODE_INCLUDES_H
#include "LEDModeIncludes.h"
#endif

#include "ArduinoJson.h"
#include "LittleFSWrapper.h"

#include "debug.h"

class LEDController : public LEDModeObserver
{

public:
  enum class State
  {
    ON,
    OFF
  };

private:
  static LEDController *instance;

  uint8_t redPin;
  uint8_t bluePin;
  uint8_t greenPin;

  RGBColor color;
  float brightness;

  std::shared_ptr<LEDMode> currentMode;

  LEDController();

  long configWriteIntervalTimer = 0;
  int configWriteInterval = 2 * 1000;
  bool isConfigWriteEnabled = true;
  State state;

  String controllerName;

public:
  static const String UUIDESPCONTROLLER;
  static RGBColor offColor;

  LEDController(const LEDController &) = delete;
  LEDController &operator=(const LEDController &) = delete;
  LEDController(LEDController &&) = delete;
  LEDController &operator=(LEDController &&) = delete;

  void Update(LEDModeUpdateMSG msg) override;

  void setAllPins(const uint8_t rPin, const uint8_t bPin, const uint8_t gPin);
  void Init();
  void trigger();

  String createConnectConfig() const;

  RGBColor getColor() const;

  void setBrightness(int value);
  void setColor(RGBColor c);

  // Config
  void writeCurrentConfig(bool skipConfigInterval = false);

  // Modes
  void setMode(std::shared_ptr<LEDMode> newLedMode);
  std::shared_ptr<LEDMode> getLEDMode();
  static LEDController *getInstance();

  // State
  State getState() const;
  void setState(State state);
  void powerOff();
  void powerOn();

  // Controle name
  String getName() const;
  void setName(String name);
};

#endif
