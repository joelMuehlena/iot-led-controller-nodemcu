#ifndef LITTLE_FS_WRAPPER_H
#define LITTLE_FS_WRAPPER_H

#include <FS.h>
#include <LittleFS.h>

namespace LittleFSWrapper
{

  inline void listDir(fs::FS &fs, const char *dirname, uint8_t levels)
  {
    Serial.printf("Directory-Listing: %s\r\n", dirname);

    File root = fs.open(dirname, "r");
    if (!root)
    { // Verzeichnis nicht vorhanden
      Serial.println("- Fehler beim Öffnen des Verzeichnisses");
      return;
    }
    if (!root.isDirectory())
    { // kein Verzeichnis, sondern Datei
      Serial.println(" - kein Verzeichnis");
      return;
    }

    File file = root.openNextFile();
    // in einer Schleife durch die Liste aller vorhandenen
    // Einträge gehen
    while (file)
    {
      if (file.isDirectory())
      {
        Serial.print("  DIR : ");
        Serial.println(file.name());
        // ist der Eintrag ein Verz., dann dessen Inhalt rekursiv
        // anzeigen, wenn maximale Tiefe noch nicht erreicht ist
        if (levels)
        {
          listDir(fs, file.name(), levels - 1);
        }
      }
      else
      {
        Serial.print("  FILE: ");
        Serial.print(file.name());
        Serial.print("\tGröße: ");
        Serial.println(file.size());
      }
      file = root.openNextFile();
    }
  }

  inline void writeJSONFile(DynamicJsonDocument &doc, fs::FS &fs, const char *path)
  {
    File file = fs.open(path, "w");
    if (!file)
    {
      Serial.printf("Cannot open file: %s\r\n", path);
      return;
    }

    serializeJson(doc, file);
  }

  inline void deleteFile(fs::FS &fs, const char *path)
  {
    Serial.printf("Lösche Datei: %s\r\n", path);
    if (fs.remove(path))
    {
      Serial.println("- Datei gelöscht");
    }
    else
    {
      Serial.println("- Datei konnte nicht gelöscht werden");
    }
  }

  inline bool readJSONFile(DynamicJsonDocument &doc, fs::FS &fs, const char *path)
  {
    File file = fs.open(path, "r");
    if (!file || file.isDirectory())
    {
      Serial.printf("Cannot open file: %s\r\n", path);
      return false;
    }

    size_t size = file.size();

    char *buffer = new char[size];

    file.readBytes(buffer, size);

    deserializeJson(doc, reinterpret_cast<const char *>(buffer));

    file.close();

    delete[] buffer;

    return true;
  }

  inline char *readFile(fs::FS &fs, const char *path)
  {
    File file = fs.open(path, "r");
    if (!file || file.isDirectory())
    {
      Serial.printf("Cannot open file: %s\r\n", path);
      return nullptr;
    }

    size_t size = file.size();

    char *buffer = new char[size];

    file.readBytes(buffer, size);

    file.close();
    return buffer;
  }

} // namespace LittleFSWrapper

#endif