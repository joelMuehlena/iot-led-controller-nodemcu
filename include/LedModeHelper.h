#ifndef LED_MODE_HELPER_HEADER
#define LED_MODE_HELPER_HEADER

#include <memory>

#include "LEDModeIncludes.h"

#include "LEDController.h"

inline std::unique_ptr<LEDMode> getModeFromName(const char *name)
{
  if (strcmp(name, "FADELEDMODE") == 0)
  {
    return std::make_unique<FadeLEDMode>();
  }
  else if (strcmp(name, "STATICLEDMODE") == 0)
  {
    return std::make_unique<StaticLEDMode>();
  }

#ifdef HAS_MIC
  else if (strcmp(name, "SINGLESOUNDLEDMODE") == 0)
  {
    return std::make_unique<SingleSoundLEDMode>();
  }
#endif

  else if (strcmp(name, "MULTISOUNDLEDMODE") == 0)
  {
    return std::make_unique<MultiSoundLEDMode>();
  }
  else
  {
    return nullptr;
  }
}

namespace LEDModeHelper
{
  inline bool changeLEDMode(const char *name)
  {
    auto oldMode = LEDController::getInstance()->getLEDMode();
    std::shared_ptr<LEDMode> newMode = std::move(getModeFromName(name));

    if (newMode == nullptr)
    {
      return false;
    }

    LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::MODE, newMode);
    oldMode->notify(msg);
    newMode->Attach(LEDController::getInstance());

    return true;
  }
};

#endif