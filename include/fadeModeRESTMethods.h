#include <ArduinoJson.h>

#include "HTTPRequest.h"
#include "HTTPResponse.h"
#include "LEDController.h"
#include "LedModeHelper.h"

#include "util.h"

void fadeModeOptions(const HTTPRequest &request, HTTPResponse &response)
{
  if (LEDController::getInstance()->getLEDMode()->getName() != "FADELEDMODE")
  {
    return response.setStatus(400)->send("ERROR THIS MODE IS NOT ACTIVE");
  }

  const auto body = request.getBody();

  auto bodyColorTo = body.find("colorTo");
  auto bodyColorFrom = body.find("colorFrom");
  auto bodyPeriod = body.find("period");

  if (bodyColorTo == body.end() && bodyColorFrom == body.end() && bodyPeriod == body.end())
  {
    DynamicJsonDocument doc(2048);
    doc["code"] = 400;
    doc["msg"] = "There was an error updating the Fademode";

    JsonObject error = doc.createNestedObject("error");
    error["msg"] = "Please provide any arguments of colorTo, colorFrom or period";
    response.setStatus(400)->json(doc);
  }

  auto mode = LEDMode::dynamicCast<FadeLEDMode *const>(LEDController::getInstance()->getLEDMode().get());

  if (bodyColorTo != body.end())
  {
    RGBColor c = util::hexToRgb((int)strtol(bodyColorTo->second.c_str(), nullptr, 16));
    mode->setColorTo(c);
  }

  if (bodyColorFrom != body.end())
  {
    RGBColor c = util::hexToRgb((int)strtol(bodyColorFrom->second.c_str(), nullptr, 16));
    mode->setColorFrom(c);
  }

  if (bodyPeriod != body.end())
  {
    mode->setPeriod(bodyPeriod->second.toInt() * 1000);
  }

  response.setStatus(200)->send("SUCCESS");
}