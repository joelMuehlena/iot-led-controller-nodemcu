#include <ArduinoJson.h>

#include "HTTPRequest.h"
#include "HTTPResponse.h"
#include "LEDController.h"
#include "LedModeHelper.h"

#include "util.h"

inline void postLevel(const HTTPRequest &request, HTTPResponse &response)
{

  auto body = request.getBody();

  auto level = body.find("level");
  if (level != body.end())
  {
    int levelInt = level->second.toInt();
    if (levelInt >= 0 && levelInt <= 100)
    {
      LEDController::getInstance()->setBrightness(levelInt);

      response.setStatus(200)->send("SUCCESS");
      return;
    }
  }

  DynamicJsonDocument doc(2048);
  doc["code"] = 400;
  doc["msg"] = "There was an error setting the Level";

  JsonObject error = doc.createNestedObject("error");
  error["msg"] = "Check the privided level data";

  response.setStatus(400)->json(doc);
}

inline void postColor(const HTTPRequest &request, HTTPResponse &response)
{

  if (LEDController::getInstance()->getLEDMode()->getName() != "STATICLEDMODE")
  {
    return response.setStatus(400)->send("ERROR THIS MODE IS NOT ACTIVE");
  }
  auto body = request.getBody();

  auto color = body.find("color");
  if (color != body.end())
  {
    RGBColor c = util::hexToRgb((int)strtol(color->second.c_str(), nullptr, 16));
    auto sMode = LEDMode::dynamicCast<StaticLEDMode *const>(LEDController::getInstance()->getLEDMode().get());
    LEDModeUpdateMSG msg(LEDModeUpdateMSG::UpdateType::COLOR, c);
    sMode->notify(msg);

    response.setStatus(200)->send("SUCCESS");
    return;
  }

  DynamicJsonDocument doc(2048);
  doc["code"] = 400;
  doc["msg"] = "There was an error setting the color";

  JsonObject error = doc.createNestedObject("error");
  error["msg"] = "Please check if a color was provided as hex color";
  response.setStatus(400)->json(doc);
}

inline void setLEDMode(const HTTPRequest &request, HTTPResponse &response)
{
  auto body = request.getBody();

  auto modeName = body.find("modeName");
  if (modeName != body.end())
  {

    if (LEDModeHelper::changeLEDMode(modeName->second.c_str()))
    {
      response.setStatus(200)->send("SUCCESS");
      return;
    }
  }

  DynamicJsonDocument doc(2048);
  doc["code"] = 400;
  doc["msg"] = "There was an error setting the mode";

  JsonObject error = doc.createNestedObject("error");
  error["msg"] = "Please check if a valid mode name was provided";
  response.setStatus(400)->json(doc);
}

inline void getLEDMode(const HTTPRequest &request, HTTPResponse &response)
{
  auto mode = LEDController::getInstance()->getLEDMode();

  DynamicJsonDocument doc(2048);
  doc["code"] = 200;
  doc["msg"] = mode->getName();

  response.setStatus(200)->json(doc);
}