#ifndef HTTP_RESPONSE_HEADER
#define HTTP_RESPONSE_HEADER

#include <WiFi.h>
#include <ArduinoJson.h>
#include <map>

class HTTPResponse
{
private:
  WiFiClient *const client;

  std::map<String, String> headers;

  boolean isFinished = false;
  boolean areHeadersWritten = false;

  int responseCode = 200;

  void finishRequest();
  boolean writeHeaders();

public:
  HTTPResponse(WiFiClient *const client);

  static String getTextFromHTTPCode(const int code);

  HTTPResponse *const addHeader(String key, String value);

  HTTPResponse *const setStatus(int code);
  void send(String text);
  void send(const char *text);
  void json(DynamicJsonDocument &json);
};

#endif
