#ifndef LED_MODE_FADE_HEADER
#define LED_MODE_FADE_HEADER

#include "LEDModeIncludes.h"

#include "NeoPixel.h"
class FadeLEDMode : public LEDMode, public NeoPixel
{

public:
  enum class Direction
  {
    FORWARDS,
    BACKWARDS
  };

private:
  RGBColor colorFrom;
  RGBColor colorTo;
  Direction dir;

public:
  FadeLEDMode();

  void setColorFrom(RGBColor c);
  void setColorTo(RGBColor c);

  void startNeoPixelFade();
  void handleMode() override;
  void configMode(DynamicJsonDocument &configData) override;
  void loopNeoPixel() override;
};

#endif