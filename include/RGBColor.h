#ifndef RGB_COLOR_HEADER
#define RGB_COLOR_HEADER

#include <Arduino.h>
#include <iostream>
#include <sstream>
#include <iomanip>

class RGBColor
{
public:
  explicit RGBColor(unsigned int r, unsigned int g, unsigned int b);
  RGBColor(unsigned int val);
  RGBColor();

  unsigned int r = 0;
  unsigned int g = 0;
  unsigned int b = 0;

  bool operator!=(const RGBColor &rgbColor);
  bool operator==(const RGBColor &rgbColor);
  RGBColor &operator+=(const int &val);

  String toString() const;
};

inline bool RGBColor::operator!=(const RGBColor &rgbColor)
{
  return this->r != rgbColor.r && this->g != rgbColor.g && this->b != rgbColor.b;
}

inline RGBColor &RGBColor::operator+=(const int &val)
{
  this->r += val;
  this->g += val;
  this->b += val;

  this->r %= 255;
  this->g %= 255;
  this->b %= 255;

  if (this->r == 0)
    this->r = 5;

  if (this->g == 0)
    this->g = 5;

  if (this->g == 0)
    this->g = 5;

  return *this;
}

inline bool RGBColor::operator==(const RGBColor &rgbColor)
{
  return !(*this != rgbColor);
}

inline String RGBColor::toString() const
{
  int hex;
  std::stringstream ss;
  ss << "#";
  ss << std::hex << std::uppercase << std::setfill('0') << std::setw(6) << (this->r << 16 | this->g << 8 | this->b);
  std::string s = ss.str();

  return String(s.c_str());
}

inline RGBColor::RGBColor() : r(100), g(100), b(100) {}

inline RGBColor::RGBColor(unsigned int val) : r(val), g(val), b(val) {}

inline RGBColor::RGBColor(unsigned int r, unsigned int g, unsigned int b) : r(r), g(g), b(b) {}

#endif
