#ifndef UTIL_HEADER
#define UTIL_HEADER

#include <Arduino.h>
#include "RGBColor.h"

namespace util
{
  RGBColor hexToRgb(int hexValue);
  RGBColor hexToRgb(String s);
  int max3(int x0, int x1, int x2);
  unsigned int abs(int a, int b);
}

#endif
