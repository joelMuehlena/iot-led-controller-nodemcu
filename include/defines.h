#ifndef DEFINES_H
#define DEFINES_H

#include <stdint.h>

static const uint8_t MIC = 1;
static const uint8_t REDLED = 2;
static const uint8_t GREENLED = 3;
static const uint8_t BLUELED = 4;

#define HAS_MIC

#endif /* DEFINES_H */