#ifndef LED_MODE_STATIC_HEADER
#define LED_MODE_STATIC_HEADER

#include "LEDModeIncludes.h"

class StaticLEDMode : public LEDMode
{

private:
  RGBColor color;

public:
  StaticLEDMode();

  void handleMode() override;
  void configMode(DynamicJsonDocument &configData) override;
  void setColor(RGBColor c);
};

#endif
